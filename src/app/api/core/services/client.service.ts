import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DARUIX_API } from 'app/app.api';
import { environment } from 'environments/environment';
import { Observable, of } from 'rxjs/index';
import { catchError} from 'rxjs/operators';

@Injectable()
export class ClientService {

  constructor(
    private http: HttpClient
  ) { }

  getClients() {
    return this.http.get(`${DARUIX_API}` + `${environment.client.getClients}`)
      .pipe(
        catchError((error) => {
            return of(error);
        })
    );
  }

}

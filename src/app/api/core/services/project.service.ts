import { Injectable } from '@angular/core';
import { DARUIX_API } from 'app/app.api';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs/index';
import { catchError} from 'rxjs/operators';

@Injectable()
export class ProjectService {

  constructor(
    private http: HttpClient
  ) { }

  getProjects() {
    return this.http.get(`${DARUIX_API}` + `${environment.project.getProjects}`)
      .pipe(
        catchError((error) => {
            return of(error);
        })
    );
  }

}

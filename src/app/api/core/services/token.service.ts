import { Injectable } from '@angular/core';

@Injectable()
export class TokenService {

  constructor() { }

  setToken(token: string) {
    sessionStorage.setItem('accessToken', btoa(token));
  }

  getToken() {
    return sessionStorage.getItem('accessToken');
  }

}

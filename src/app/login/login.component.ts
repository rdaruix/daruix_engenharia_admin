import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { TokenService } from 'app/api/core/services/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(
              private fb: FormBuilder,
              private loginService: LoginService,
              private router: Router,
              private tokenService: TokenService
  ) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: this.fb.control('', [Validators.required, Validators.email]),
      password: this.fb.control('', [Validators.required])
    })
  }

  login() {
    this.loginService.login(this.loginForm.value.email, this.loginForm.value.password)
                      .subscribe(
                        (user) => {
                          this.tokenService.setToken(user.token);
                          this.router.navigateByUrl('admin');
                        })
  }

}

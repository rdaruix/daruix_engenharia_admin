import {Injectable} from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs/index';
import { catchError, retry } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';

import {User} from './user.model';
import { DARUIX_API } from 'app/app.api';
import { environment } from 'environments/environment';
import { TokenService } from 'app/api/core/services/token.service';

@Injectable()
export class LoginService {
    user: User;
    constructor(
        private http: HttpClient
    ) {}

    isLoggedIn(): boolean {
        return this.user !== undefined;
    }

    login(email: string, password: string){
        const user = {email: email, password: password};
        return this.http.post<User>(`${DARUIX_API}` + `${environment.login}`, user)
        .pipe(
            catchError((error) => {
                return of(error);
            })
        );
    }
}

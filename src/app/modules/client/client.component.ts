import { Component, OnInit } from '@angular/core';
import { ClientService } from 'app/api/core/services/client.service';
import { error } from '@angular/compiler/src/util';
import { ProjectService } from 'app/api/core/services/project.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {

  public clients;
  public projects;

  constructor(
    private clientService: ClientService,
    private projectService: ProjectService
  ) { }

  ngOnInit() {
    this.clientService.getClients().subscribe(
      (data) => {
        this.clients = data.data;
      }, error => {
        console.log(error);
      }
    );

    this.projectService.getProjects().subscribe(
      (data) => {
        this.projects = data.data;
      }
    )

  }

}

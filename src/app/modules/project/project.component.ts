import { Component, OnInit } from '@angular/core';
import { ClientService } from 'app/api/core/services/client.service';
import { FormGroup, FormControl, NgModel } from '@angular/forms';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  public clients;
  public clientsSettings = {};

  public projectForm = new FormGroup({
    name: new FormControl(''),
    cliente_id: new FormControl('')
  });


  constructor(
    private clientService: ClientService
    ) { }

  ngOnInit() {
    this.clientService.getClients().subscribe(
      (data) => {
        this.clients = data.data;
      }, error => {
        console.log(error);
      }
    );

    this.clientsSettings = {
      text: 'Clientes',
      singleSelection: true,
      showCheckbox: false,
      labelKey: 'name'
    };
  }

  onItemSelect(item: any) {
    this.projectForm.controls.cliente_id.setValue(item.id);
  }

  onSubmit() {
    console.log(this.projectForm.value);
  }
}
